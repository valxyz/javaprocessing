import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import processing.core.PVector;
import peasy.*;
import peasy.org.apache.commons.math.MathException;
import peasy.org.apache.commons.math.geometry.*;

@SuppressWarnings("unused")

public class Cube extends PApplet{
    

    //Processing Rutine
    
    public float time = 0.0f;
    public int dpi = 1;
    public float wscale = 50.0f;
    public String colorbackground = "#0E1012"; //14, 16, 18
    public String colorhud = "#E5DCDC"; //229, 220, 220

    PeasyCam camera;
    float fov = 75.0f;
    PFont dfont;

    @Override  
    public void settings(){
      
      size(1080,620,P3D);
      pixelDensity(dpi);
      smooth(2);
      
      return;
    }

    @Override
    public void setup() {
        
        frameRate(60);
        textSize(64);
        textAlign(LEFT);
        //rectMode (CENTER);

        perspective( (fov)*(PI/180.0f), (width+0.0f)/(height+0.0f), 0.1f, 5000.0f);
        camera = new PeasyCam(this, 500);
      
      return;  
    }
    
    
    @Override
    public void draw() {
        

        push();
        

        background(colorbackground);
        updateconfig();
        float t = time;
        
        /*lightFalloff(0.0, 0.0, 0.0000064);
        directionalLight(155, 155, 155, 0, -1, 0);
        pointLight(100,100,100, -30, -0, -0);
        pointLight(100,100,100, -0, -30, -0);
        pointLight(100,100,100, -0, -0, -30); */
        
        ambientLight(112,112,112);
        directionalLight(100, 100, 100, 1, 0, 0);
        lightSpecular(90,90,90);
        directionalLight(100, 100, 100, 0, -1, 1);
        lightSpecular(50,50,50);
        
        pushStyle();
        emissive(34,34,35);
        dgridmesh(0,0,0, 50,50, 30,30,  "#222223", 0.01f, 4);  //#222223
        emissive(63,64,66);//#3F4042
        dgridmesh(0,0,0, 10,10, 30,30,  "#3F4042", 0.012f, 4);  //#3F4042
        popStyle();
        
        fill("#F0F0F0");
        rect(-8,-8,16,16);
        
        fill("#A3BC41");
        stroke(0);
        strokeWeight(0.02f);
        //rotateY(t*0.3);
        //rotateZ(t*0.8);
        //box(2,2,16);
        dbox(0,0,2.5f, 5.0f);
        
        //strokeWeight(0.01f);
        //dsphere(8,0,3.0f, 3.0f, 64,32);
        
        
        pop();
        
        
        hud();
        

        return;
    }

    


    
    
    //Utils
    
    public PVector hextorgb(String hexcolor) {
    
        PVector col = new PVector();

        if (hexcolor.charAt(0) == '#') {
            hexcolor = hexcolor.substring(1);
        }

        if (hexcolor.length() != 6) {
            throw new IllegalArgumentException("Invalid hex color format");
        }
        
        try {
            
            int _r = Integer.parseInt(hexcolor.substring(0, 2), 16);
            int _g = Integer.parseInt(hexcolor.substring(2, 4), 16);
            int _b = Integer.parseInt(hexcolor.substring(4, 6), 16);
            
            col.set(_r, _g, _b);

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid hex color format", e);
        }
        
        
        return col;
}
    
    void stroke(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        stroke(col.x, col.y, col.z);
     
        return;
    }
    
    void fill(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        fill(col.x, col.y, col.z);
     
        return;
    }
    
    void background(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        background(col.x, col.y, col.z);
     
        return;
    }
    
    

    //config functions

    void initconfig(){
    
    return;
    }

    void prefconfig(){
    
    //translate (width/2.0, height/2.0);
    scale(wscale,-wscale, wscale);
    
    /*
    
    applyMatrix(1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0);  
    
    */
    
    
    return;
    }

    void updateconfig(){
    
    prefconfig();
    time = millis()/1000.0f;
    
    return;
    }

    void hud(){
    
    camera.beginHUD();
    
    float offset = 7.0f;
    //image(hudcanvas, width - hudcanvas.width - offset, offset);
    
    fill(255,20);
    rect(0.0f,0.0f, 132.0f + offset,62.0f+offset);
    
    textSize(14.0f);
    fill (colorhud);
    noStroke();
    text(nfc(frameRate,2)+" fps", 5+offset,16+offset);
    text(nfc(millis()/1000.0f,2)+" s", 5+offset,32+offset);
    text("distance: "+nfc((float)camera.getDistance(),2), 5+offset,48+offset);
    
    camera.endHUD();
    
    return;
    }


    @Override
    public void keyPressed() {
    
    //RotationOrder ro = new RotationOrder("XYZ", Vector3D.plusI, Vector3D.plusJ, Vector3D.plusK);
    
    Vector3D u1 = new Vector3D(0.0,0.0,0.0);
    Vector3D v1 = new Vector3D(0.0,0.0,0.0);
    Vector3D u2 = new Vector3D(0.0,0.0,0.0);
    Vector3D v2 = new Vector3D(0.0,0.0,0.0);
    Rotation rotation;
    Vector3D center;
    double distance = 500.0;
    CameraState camstate;
    
    switch(key){
        
        case '1':
    
        rotation = new Rotation(1.0,0.0,0.0,0.0,true); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        case '2':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,0.0,-1.0);
        rotation = new Rotation(u1,v1); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        
        case '3':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,1.0,0.0);
        rotation = new Rotation(u1,v1); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        case '4':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,1.0,0.0);

        u2 = new Vector3D(1.0,0.0,0.0);
        v2 = new Vector3D(0.0,0.0,-1.0);
        
        rotation = new Rotation(u1,v1,u2,v2); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        
        case '5':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,1.0,0.0);
        
        u2 = new Vector3D(0.0,-1.0,0.0);
        v2 = new Vector3D(0.0,0.0,-1.0);
        
        rotation = new Rotation(u1,v1,u2,v2); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        case '6':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,1.0,0.0);

        u2 = new Vector3D(-1.0,0.0,0.0);
        v2 = new Vector3D(0.0,0.0,-1.0);
        
        rotation = new Rotation(u1,v1,u2,v2); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
        case '7':
        
        u1 = new Vector3D(0.0,0.0,1.0);
        v1 = new Vector3D(0.0,1.0,0.0);
        rotation = new Rotation(u1,v1); 
        center = new Vector3D(0,0,0);
        camstate = new CameraState(rotation, center, distance);
        camera.setState(camstate,400);
        break;
        
    }
    
    
    return;
    }


    //draw functions

    void dsphere(float x, float y, float z, float r){
    
    push();
        translate(x,y,z);
        rotateX(PI/2.0f);
        sphere(r);
    pop();
    
    return;
    }

    void dsphere(float x, float y, float z, float r, int du, int dv){
    
    push();
        translate(x,y,z);
        rotateX(PI/2.0f);
        sphereDetail(du,dv);
        sphere(r);
    pop();
    
    return;
    }

    void dbox(float x, float y, float z, float size){
    
    push();
        translate(x,y,z);
        box(size);
    pop();
    
    return;
    }

    void dbox(float x, float y, float z, float a, float b, float c){
    
    push();
        translate(x,y,z);
        box(a,b,c);
    pop();
    
    return;
    }

    void dcylinder(float x, float y, float z, float r, float l, float subdivision){

    push();
        translate(x,y,z);
        subdivision = abs(subdivision) == 0.0f ? 32.0f : abs(subdivision);
        float dn = (2.0f*PI)/subdivision;
            
        for(float i = 0.0f ; i < 2.0*PI ; i += dn){
        
        float _x1 = r*cos(i);
        float _y1 = r*sin(i);
        float _x2 = r*cos(i+dn);
        float _y2 = r*sin(i+dn);
        
        float _z1 =  l*0.5f;
        float _z2 = -l*0.5f;
            
        //lateral faces
        beginShape();
            vertex(_x1, _y1, _z1);
            vertex(_x2, _y2, _z1);
            vertex(_x2, _y2, _z2);
            vertex(_x1, _y1, _z2);
        endShape(CLOSE);
        
        //top faces
        beginShape();
            vertex(_x1, _y1, _z1);
            vertex(0,0, _z1);
            vertex(_x2, _y2, _z1);
        endShape(CLOSE);
        
        //bottom faces
        beginShape();
            vertex(_x1, _y1, _z2);
            vertex(0,0, _z2);
            vertex(_x2, _y2, _z2);
        endShape(CLOSE);
        
        }
        
        
    pop();
    
    return;
    }


    void dcylinder(float x, float y, float z, float r, float l, float rx, float ry, float rz, float subdivision){

    pushMatrix();
        
        translate(x,y,z);
        rotateZ(rz);
        rotateY(rx);
        rotateX(ry);
            
        subdivision = abs(subdivision) == 0.0f ? 32.0f : abs(subdivision);
        float dn = (2.0f*PI)/subdivision;
        
        for(float i = 0.0f ; i < 2.0*PI ; i += dn){
        
        float _x1 = r*cos(i);
        float _y1 = r*sin(i);
        float _x2 = r*cos(i+dn);
        float _y2 = r*sin(i+dn);
        
        float _z1 =  l*0.5f;
        float _z2 = -l*0.5f;
            
        //lateral faces
        beginShape();
            vertex(_x1, _y1, _z1);
            vertex(_x2, _y2, _z1);
            vertex(_x2, _y2, _z2);
            vertex(_x1, _y1, _z2);
        endShape(CLOSE);
        
        //top faces
        beginShape();
            vertex(_x1, _y1, _z1);
            vertex(0,0, _z1);
            vertex(_x2, _y2, _z1);
        endShape(CLOSE);
        
        //bottom faces
        beginShape();
            vertex(_x1, _y1, _z2);
            vertex(0,0, _z2);
            vertex(_x2, _y2, _z2);
        endShape(CLOSE);
        
        }
        
        
    popMatrix();
    
    return;
    }


    void dcone(float x, float y, float z, float r1, float r2, float l, float rx, float ry, float rz, float subdivision){

    push();
        
        translate(x,y,z);
        rotateZ(rz);
        rotateY(rx);
        rotateX(ry);
            
        subdivision = abs(subdivision) == 0.0f ? 32.0f : abs(subdivision);
        float dn = (2.0f*PI)/subdivision;
        
        /*
        for(float i = 0.0 ; i < 2.0*PI ; i += dn){
        
        float x1 = r1*cos(i);
        float y1 = r1*sin(i);
        float x2 = r2*cos(i);
        float y2 = r2*sin(i);
        
        float z1 =  l*0.5;
        float z2 = -l*0.5;
        
        stroke(#7B50DB);
        strokeWeight(0.025);
        point(x1,y1,z1);
        point(x2,y2,z2);
        
        line(x1,y1,z1, 0,0,z1);
        line(x1,y1,z1, x2,y2,z2);
        line(x2,y2,z2, 0,0,z2);
        
        }*/
        
        
        for(float i = 0.0f ; i < 2.0f*PI ; i += dn){
        
        float x1 = r1*cos(i);
        float y1 = r1*sin(i);
        float _x1 = r1*cos(i+dn);
        float _y1 = r1*sin(i+dn);
        
        float x2 = r2*cos(i);
        float y2 = r2*sin(i);
        float _x2 = r2*cos(i+dn);
        float _y2 = r2*sin(i+dn);
        
        float z1 =  l*0.5f;
        float z2 = -l*0.5f;
            
        //lateral faces
        beginShape();
            vertex(x1, y1, z1);
            vertex(_x1, _y1, z1);
            vertex(_x2, _y2, z2);
            vertex(x2, y2, z2);
        endShape(CLOSE);
        
        //top faces
        beginShape();
            vertex(x1, y1, z1);
            vertex(0,0, z1);
            vertex(_x1, _y1, z1);
        endShape(CLOSE);    
        
        //bottom faces
        beginShape();
            vertex(x2, y2, z2);
            vertex(0,0, z2);
            vertex(_x2, _y2, z2);
        endShape(CLOSE);
        
        }
        
        
        
    pop();
    
    return;
    }

    void dcone(float x, float y, float z, float r1, float r2, float l, float rx, float ry, float rz, float subdivision , String center){

    float z1 =  l*0.5f;
    float z2 = -l*0.5f;
    
    push();
        
        translate(x,y,z);
        
        switch(center.toUpperCase()){
            
            case "TOP":  z1 = 0.0f; z2 = -l;
                        break;
                        
            case "CENTER": break;
                        
            case "BASE": z1 = l; z2 = 0.0f;
                        break; 
                        
            default: 
                    break;
            
        }
        
        
        rotateZ(rz);
        rotateY(rx);
        rotateX(ry);
            
        subdivision = abs(subdivision) == 0.0f ? 32.0f : abs(subdivision);
        float dn = (2.0f*PI)/subdivision;
        
        
        for(float i = 0.0f ; i < 2.0*PI ; i += dn){
        
        float x1 = r1*cos(i);
        float y1 = r1*sin(i);
        float _x1 = r1*cos(i+dn);
        float _y1 = r1*sin(i+dn);
        
        float x2 = r2*cos(i);
        float y2 = r2*sin(i);
        float _x2 = r2*cos(i+dn);
        float _y2 = r2*sin(i+dn);
            
        //lateral faces
        beginShape();
            vertex(x1, y1, z1);
            vertex(_x1, _y1, z1);
            vertex(_x2, _y2, z2);
            vertex(x2, y2, z2);
        endShape(CLOSE);
        
        //top faces
        beginShape();
            vertex(x1, y1, z1);
            vertex(0,0, z1);
            vertex(_x1, _y1, z1);
        endShape(CLOSE);    
        
        //bottom faces
        beginShape();
            vertex(x2, y2, z2);
            vertex(0,0, z2);
            vertex(_x2, _y2, z2);
        endShape(CLOSE);
        
        }
    
    pop();
    
    return;
    }

    void dfrustum(float x, float y, float z, float w1, float h1, float w2, float h2, float l, float rx, float ry, float rz){
    
    //top
    float tx1 = -w1*0.5f;
    float ty1 = h1*0.5f;
    float tz1 = l*0.5f;
    
    float tx2 = w1*0.5f;
    float ty2 = h1*0.5f;
    float tz2 = l*0.5f;
    
    float tx3 = w1*0.5f;
    float ty3 = -h1*0.5f;
    float tz3 = l*0.5f;
    
    float tx4 = -w1*0.5f;
    float ty4 = -h1*0.5f;
    float tz4 = l*0.5f;
    
    //bottom
    float bx1 = -w2*0.5f;
    float by1 = h2*0.5f;
    float bz1 = -l*0.5f;
    
    float bx2 = w2*0.5f;
    float by2 = h2*0.5f;
    float bz2 = -l*0.5f;
    
    float bx3 = w2*0.5f;
    float by3 = -h2*0.5f;
    float bz3 = -l*0.5f;
    
    float bx4 = -w2*0.5f;
    float by4 = -h2*0.5f;
    float bz4 = -l*0.5f;
    
    //noFill();
    
    push();
        
        translate(x,y,z);

        rotateX(rx);
        rotateY(ry);
        rotateZ(rz);
        
        
        //top face
        beginShape();
        vertex(tx1, ty1, tz1);
        vertex(tx2, ty2, tz2);
        vertex(tx3, ty3, tz3);
        vertex(tx4, ty4, tz4);
        endShape(CLOSE);
        
        noFill();
        
        //bottom face
        beginShape();
        vertex(bx1, by1, bz1);
        vertex(bx2, by2, bz2);
        vertex(bx3, by3, bz3);
        vertex(bx4, by4, bz4);
        endShape(CLOSE);
        
        //up face
        beginShape();
        vertex(tx1, ty1, tz1);
        vertex(tx2, ty2, tz2);
        vertex(bx2, by2, bz2);
        vertex(bx1, by1, bz1);
        endShape(CLOSE);
        
        //below face
        beginShape();
        vertex(tx4, ty4, tz4);
        vertex(tx3, ty3, tz3);
        vertex(bx3, by3, bz3);
        vertex(bx4, by4, bz4);
        endShape(CLOSE);
        

        
        //line(tx1, ty1, tz1, bx1, by1, bz1);
        //line(tx2, ty2, tz2, bx2, by2, bz2);
        //line(tx3, ty3, tz3, bx3, by3, bz3);
        //line(tx4, ty4, tz4, bx4, by4, bz4);
        
        //left face
        beginShape();
        vertex(tx1, ty1, tz1);
        vertex(bx1, by1, bz1);
        vertex(bx4, by4, bz4);
        vertex(tx4, ty4, tz4);
        endShape(CLOSE);
        
        //right face
        beginShape();
        vertex(tx2, ty2, tz2);
        vertex(bx2, by2, bz2);
        vertex(bx3, by3, bz3);
        vertex(tx3, ty3, tz3);
        endShape(CLOSE);
        
        
    pop();
    
    return;
    }

    void dfrustum(float x, float y, float z, float w1, float h1, float w2, float h2, float l, float rx, float ry, float rz , String rtype){
    
        //top
        float tx1 = -w1*0.5f;
        float ty1 = h1*0.5f;
        float tz1 = l*0.5f;
        
        float tx2 = w1*0.5f;
        float ty2 = h1*0.5f;
        float tz2 = l*0.5f;
        
        float tx3 = w1*0.5f;
        float ty3 = -h1*0.5f;
        float tz3 = l*0.5f;
        
        float tx4 = -w1*0.5f;
        float ty4 = -h1*0.5f;
        float tz4 = l*0.5f;
        
        //bottom
        float bx1 = -w2*0.5f;
        float by1 = h2*0.5f;
        float bz1 = -l*0.5f;
        
        float bx2 = w2*0.5f;
        float by2 = h2*0.5f;
        float bz2 = -l*0.5f;
        
        float bx3 = w2*0.5f;
        float by3 = -h2*0.5f;
        float bz3 = -l*0.5f;
        
        float bx4 = -w2*0.5f;
        float by4 = -h2*0.5f;
        float bz4 = -l*0.5f;
        
        //noFill();
        
        push();
        
        translate(x,y,z);
        
        switch(rtype.toUpperCase()){
            
            case "XYZ":  rotateX(rx); 
                        rotateY(ry);
                        rotateZ(rz);
                        break;
                        
            case "ZYX":  rotateZ(rz); 
                        rotateY(ry);
                        rotateX(rx);
                        break;
                        
            case "ZXY":  rotateZ(rz); 
                        rotateX(rx);
                        rotateY(ry);
                        break; 
                        
            default: rotateX(rx);
                    rotateY(ry);
                    rotateZ(rz);
                    break;
            
        }
        
        
        //top face
        beginShape();
            vertex(tx1, ty1, tz1);
            vertex(tx2, ty2, tz2);
            vertex(tx3, ty3, tz3);
            vertex(tx4, ty4, tz4);
        endShape(CLOSE);
        
        
        //bottom face
        beginShape();
            vertex(bx1, by1, bz1);
            vertex(bx2, by2, bz2);
            vertex(bx3, by3, bz3);
            vertex(bx4, by4, bz4);
        endShape(CLOSE);
        
        //up face
        beginShape();
            vertex(tx1, ty1, tz1);
            vertex(tx2, ty2, tz2);
            vertex(bx2, by2, bz2);
            vertex(bx1, by1, bz1);
        endShape(CLOSE);
        
        //below face
        beginShape();
            vertex(tx4, ty4, tz4);
            vertex(tx3, ty3, tz3);
            vertex(bx3, by3, bz3);
            vertex(bx4, by4, bz4);
        endShape(CLOSE);
        
    
        //line(tx1, ty1, tz1, bx1, by1, bz1);
        //line(tx2, ty2, tz2, bx2, by2, bz2);
        //line(tx3, ty3, tz3, bx3, by3, bz3);
        //line(tx4, ty4, tz4, bx4, by4, bz4);
        
        //left face
        beginShape();
            vertex(tx1, ty1, tz1);
            vertex(bx1, by1, bz1);
            vertex(bx4, by4, bz4);
            vertex(tx4, ty4, tz4);
        endShape(CLOSE);
        
        //right face
        beginShape();
            vertex(tx2, ty2, tz2);
            vertex(bx2, by2, bz2);
            vertex(bx3, by3, bz3);
            vertex(tx3, ty3, tz3);
        endShape(CLOSE);
        
        
        pop();
    
        return;
    }

void dline(float x1, float y1, float z1, float x2, float y2, float z2, float r){
  
  float d = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
  float theta = azimuth(x2-x1, y2-y1);
  float phi = elevation(x2-x1,y2-y1,z2-z1);
  Vector pos = linevec(new Vector(x1,y1,z1), new Vector(x2-x1,y2-y1,z2-z1), 0.5f); 
  dcylinder(pos.x,pos.y,pos.z,r,d, 0, phi-PI/2.0f, theta-PI/2.0f, 32.0f);
  
  return;
}

void dline(float x1, float y1, float z1, float x2, float y2, float z2, float r, float subdivision){
  
  float d = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
  float theta = azimuth(x2-x1, y2-y1);
  float phi = elevation(x2-x1,y2-y1,z2-z1);
  Vector pos = linevec(new Vector(x1,y1,z1), new Vector(x2-x1,y2-y1,z2-z1), 0.5f); 
  
  dcylinder(pos.x,pos.y,pos.z,r,d, 0.0f, phi-PI/2.0f, theta-PI/2.0f, subdivision);
  
  return;
}

void dline(float x1, float y1, float z1, float x2, float y2, float z2, float r, float subdivision, float a){
  
  float d = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
  float theta = azimuth(x2-x1, y2-y1);
  float phi = elevation(x2-x1,y2-y1,z2-z1);
  Vector pos = linevec(new Vector(x1,y1,z1), new Vector(x2-x1,y2-y1,z2-z1), 0.5f); 
  
  dcylinder(pos.x,pos.y,pos.z,r,d, a, phi-PI/2.0f, theta-PI/2.0f, subdivision);
  
  return;
}

    void dvector(float x1, float y1, float z1, float x2, float y2, float z2, float r){
    
    float k = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
    float theta = azimuth(x2-x1, y2-y1);
    float phi = elevation(x2-x1,y2-y1,z2-z1);
    
    float l = 7.1f*(r);
    float r1 = 0.0f;
    float r2 = 3.45f*(r);
    
    float px = 0;
    float py = 0;
    float pz = 0;
    float qx = x2-x1;
    float qy = y2-y1;
    float qz = z2-z1;
    
    float d = l;
    float td = (px*px + py*py + pz*pz - px*qx - py*qy - pz*qz + sqrt((d*d - 2*d*k + k*k)*px*px + (d*d - 2*d*k + k*k)*py*py + (d*d - 2*d*k + k*k)*pz*pz - 2*(d*d - 2*d*k + k*k)*px*qx + (d*d - 2*d*k + k*k - py*py - pz*pz)*qx*qx + (d*d - 2*d*k + k*k - px*px - pz*pz)*qy*qy + (d*d - 2*d*k + k*k - px*px - py*py)*qz*qz + 2*(px*py*qx - (d*d - 2*d*k + k*k)*py)*qy + 2*(px*pz*qx + py*pz*qy - (d*d - 2*d*k + k*k)*pz)*qz))/(px*px + py*py + pz*pz - 2*px*qx + qx*qx - 2*py*qy + qy*qy - 2*pz*qz + qz*qz);  
    
    Vector pos = linevec(new Vector(x1,y1,z1), new Vector(x2-x1,y2-y1,z2-z1), td); 
    dline(x1,y1,z1, pos.x, pos.y, pos.z, r);
    
    

    
    dcone(x2,y2,z2,r1,r2,l,  0,phi-PI/2.0f,theta-PI/2.0f, 32.0f, "TOP");
    
    /*stroke(#E849B3);
    strokeWeight(0.15);
    line(x1,y1,z1, x2,y2,z2); */
    
    return;
    }




    void dimage(float x, float y, float z, float s, PImage img){
    
    push();
    translate(x,y,z);
    scale(s,-s,s);
    image(img, 0, 0);
    pop();
    
    return;
    }

    void daxis2d(float x, float y, float lx, float ly, float thicknessline, float thicknesspoint, String colaxis){
    
    String colxpositive = "#FF4747";
    String colypositive = "#7BFF47"; //#4782FF;
    String colxaxis = colaxis;
    String colyaxis = colaxis;
    
    //x  
    stroke(colxaxis);
    strokeWeight (thicknessline);
    line(x-lx/2.0f,y,  x+lx/2.0f,y);
    
    strokeWeight (thicknesspoint);
    point(x-lx/2.0f,0.0f);
    
    stroke(colxpositive);
    strokeWeight (thicknesspoint);
    point(x+lx/2.0f,0.0f);
    
    //y  
    stroke(colyaxis);
    strokeWeight (thicknessline);
    line(x,y-ly/2.0f,  x,y+ly/2.0f);
    
    strokeWeight (thicknesspoint);
    point(0.0f,y-ly/2.0f);
    
    stroke(colypositive);
    strokeWeight (thicknesspoint);
    point(0.0f,y+ly/2.0f);
    
    return;
    }


    void daxis3d(float x, float y, float z, float lx, float ly, float lz, float thicknessline, float thicknesspoint, String colaxis){
    
    String colxpositive = "#FF4747";
    String colypositive = "#7BFF47"; //#4782FF;
    String colzpositive = "#4877FF";
    String colnegative = colaxis;
    
    //x  
    stroke(colaxis);
    strokeWeight (thicknessline);
    line(x-lx/2.0f,y,z,  x+lx/2.0f,y,z);
    
    strokeWeight (thicknesspoint);
    point(x-lx/2.0f,y,z);
    
    stroke(colxpositive);
    strokeWeight (thicknesspoint);
    point(x+lx/2.0f,y,z);
    
    //y  
    stroke(colaxis);
    strokeWeight (thicknessline);
    line(x,y-ly/2.0f,z,  x,y+ly/2.0f,z);
    
    strokeWeight (thicknesspoint);
    point(x,y-ly/2.0f,z);
    
    stroke(colypositive);
    strokeWeight (thicknesspoint);
    point(x,y+ly/2.0f,z);
    
    
    
    //z
    stroke(colaxis);
    strokeWeight (thicknessline);
    line(x,y,z-lz/2.0f,  x,y,z+lz/2.0f);
    
    strokeWeight (thicknesspoint);
    point(x,y,z-lz/2.0f);
    
    stroke(colzpositive);
    strokeWeight (thicknesspoint);
    point(x,y,z+lz/2.0f);
    
    return;
    }

    void daxis(float x, float y, float z, float lx, float ly, float lz, float rsize, float rpoint, String colaxis){
    
        String colxpositive = "#FF4747";
        String colypositive = "#7BFF47"; //#4782FF;
        String colzpositive = "#4877FF";
        String colnegative = colaxis;
        
        //x
        noStroke();
        fill(colnegative);
        dline(x-lx/2.0f,y,z,  x+lx/2.0f,y,z,  rsize);
        dsphere(x-lx/2.0f,y,z, rpoint);
        fill(colxpositive);
        dsphere(x+lx/2.0f,y,z, rpoint);
        
        //y  
        fill(colaxis);
        dline(x,y-ly/2.0f,z,  x,y+ly/2.0f,z, rsize);
        dsphere(x,y-ly/2.0f,z, rpoint);
        fill(colypositive);
        dsphere(x,y+ly/2.0f,z, rpoint);
        
        //z
        fill(colaxis);
        dline(x,y,z-lz/2.0f,  x,y,z+lz/2.0f, rsize);
        dsphere(x,y,z-lz/2.0f, rpoint);
        fill(colzpositive);
        dsphere(x,y,z+lz/2.0f, rpoint);
        
        return;
    }


    void dgrid(float x, float y, float col, float row, float w, float h, String linecolor, float linethickness){
    
    stroke(linecolor);
    strokeWeight(linethickness);
    
    //vertical lines
    for (float ic = -w/2.0f ; ic <= w/2.0f ; ic += w/col ){
        line (x+ic,y+-h/2.0f, x+ic , y+h/2.0f);    
    }
    
    //horizontal lines
    for (float ir = -h/2.0f ; ir <= h/2.0f ; ir += h/row ){    
        line (-w/2.0f + x , ir + y , w/2.0f + x , ir + y);
    }
    
    return;
    }

    void dgridmesh(float x, float y, float col, float row, float w, float h, String linecolor, float linethickness){
    
    //stroke(linecolor);
    //strokeWeight(linethickness);
    
    fill(linecolor);
    noStroke();
    
    //vertical lines
    for (float ic = -w/2.0f ; ic <= w/2.0f ; ic += w/col ){
        dline(x+ic,y+-h/2.0f,0, x+ic , y+h/2.0f,0, linethickness);    
    }
    
    //horizontal lines
    for (float ir = -h/2.0f ; ir <= h/2.0f ; ir += h/row ){    
        dline(-w/2.0f + x , ir + y , 0,  w/2.0f + x , ir + y, 0, linethickness);
    }
    
    return;
    }

    void dgridmesh(float x, float y, float z, float col, float row, float w, float h, String linecolor, float linethickness, float subdivision){
    
        //stroke(linecolor);
        //strokeWeight(linethickness);
        
        fill(linecolor);
        noStroke();
        
        //vertical lines
        for (float ic = -w/2.0f ; ic <= w/2.0f ; ic += w/col ){
            dline(x+ic,y+-h/2.0f,z, x+ic , y+h/2.0f,z, linethickness, subdivision, PI/4.0f);    
        }
        
        //horizontal lines
        for (float ir = -h/2.0f ; ir <= h/2.0f ; ir += h/row ){    
            dline(-w/2.0f + x , ir + y , z,  w/2.0f + x , ir + y, z, linethickness, subdivision, PI/4.0f);
        }
        
        return;
    }


    void dgridmesh(float x, float y, float z, float rx, float ry, float rz, float col, float row, float w, float h, String linecolor, float linethickness, float subdivision){
    
        push();
        
        rotateZ(rz);
        rotateY(ry);
        rotateX(rx);
        
        fill(linecolor);
        noStroke();
        
        //vertical lines
        for (float ic = -w/2.0f ; ic <= w/2.0f ; ic += w/col ){
            dline(x+ic,y+-h/2.0f,z, x+ic , y+h/2.0f,z, linethickness, subdivision);    
        }
        
        //horizontal lines
        for (float ir = -h/2.0f ; ir <= h/2.0f ; ir += h/row ){    
            dline(-w/2.0f + x , ir + y , z,  w/2.0f + x , ir + y, z, linethickness, subdivision);
        }
        
        pop();
        
        return;
    }


    void dtext(String str, float x, float y, float size){
    
        push();
        scale(1.0f,-1.0f);
        textSize(size);
        text(str,x,-y);
        pop();
    
    return;
    }

    void dtext(String str, float x, float y, float z, float size){
    
        push();
        scale(1.0f,-1.0f);
        translate(x,-y,z);
        textSize(size);
        text(str,0,0,0);
        pop();
    
    return;
    }

    void dtext(String str, float x, float y, float z, float rx, float ry, float rz, float size){
    
        push();
        scale(1.0f,-1.0f);
        translate(x,-y,z);
        rotateX(rx);
        rotateY(ry);
        rotateZ(rz); 
        textSize(size);
        text(str,0,0,0);
        pop();
        
    return;
    }




    //Math Functions


    float sinh(float x){  
        return (exp(x)-exp(-x))/2.0f;
    }

    float cosh( float x){  
        return (exp(x)+exp(-x))/2.0f;
    }

    float coth(float x){  
        return cosh(x)/sinh(x);
    }

    float sech(float x){  
        return 1/cosh(x);
    }

    float csch( float x){  
        return 1/sinh(x);
    }

    float tanh(float x){
        return (exp(x)-exp(-x))/(exp(x)+exp(-x));
    }

    float tnh(float x){
    
        float a = 8.0f;
        return x <= -a ? -1.0f :  a <= x ? 1.0f : tanh(x);
    }

    float sigmoid(float x){
    
        return 1.0f/(1.0f+exp(-x));
    }

    float sgm(float x){
        float a = 8.0f;
        return x <= -a ? 0.0f :  a <= x ? 1.0f : sigmoid(2.0f*x);
    }

    float sgn(float x){
        return ( x < 0 ? -1 : (x > 0 ? 1 : 0) );
    }

    float step(float a, float x){ 
        return (x<a) ? 0.0f:1.0f; 
    }

    Vector add(Vector u, Vector v){
        return new Vector(u.x+v.x, u.y+v.y, u.z+v.z,  u.w+v.w);
    }

    Vector add(float a, Vector v){
        return new Vector(a+v.x, a+v.y, a+v.z,  a+v.w);
    }

    Vector sub(Vector u, Vector v){
        return new Vector(u.x-v.x, u.y-v.y, u.z-v.z,  u.w-v.w);
    }

    Vector mult(float a, Vector v){
        return new Vector(a*v.x, a*v.y, a*v.z, a*v.w);
    }

    Vector norm(Vector v){
        float d = v.length();
        return mult(1.0f/d,v);
    }

    Vector normalize(Vector v){
        float d = v.length();
        return mult(1.0f/d,v);
    }

    Vector linevec(Vector p, Vector u, float t){
        return add(p,mult(t,u));
    }

    Vector lerpvec(Vector a, Vector b, float t){
    
        float x0 = a.x;
        float y0 = a.y;
        float z0 = a.z;
        float x1 = b.x;
        float y1 = b.y;
        float z1 = b.z;
        
        //return [lerpnum(p0[0],p1[0],t),lerpnum(p0[1],p1[1],t),lerpnum(p0[2],p1[2],t)];
        
        return new Vector ( (1-t)*x0 + t*x1, (1-t)*y0 + t*y1 , (1-t)*z0 + t*z1 );
    }

    float lerpnum(float a , float b, float t){
    
        t = abs(t) > 1.0f ? 1.0f : abs(t);
        return (1-t)*a + b*t;
    }

    float length(float x, float y){
        return mag(x,y);
    }

    float mod(float a, float b){
        return  a-b*floor(a/b);
    }

    float azimuth(float x, float y){
        float d = length(x,y);    
        return d == 0.0f ? 0.0f : ( 0.0f <= y ? acos(x/d) : 2.0f*PI - acos(x/d) );
    }

    float azimuth(Vector v){
        return azimuth(v.x,v.y);
    }


    float elevation(float x, float y, float z){
        float d = length(x,y);
        return d == 0.0f ? (z < 0.0f ? -PI/2.0f : ( z == 0.0f ? 0.0f : PI/2.0f) ) : atan(z/d);
    }

    float elevation(Vector v){
        return elevation(v.x,v.y,v.z);
    }

    Vector twilightshifted(float t) {
    
        Vector c0 = new Vector(0.120488f, 0.047735f, 0.106111f);
        Vector c1 = new Vector(5.175161f, 0.597944f, 7.333840f);
        Vector c2 = new Vector(-47.426009f, -0.862094f, -49.143485f);
        Vector c3 = new Vector(197.225325f, 47.538667f, 194.773468f);
        Vector c4 = new Vector(-361.218441f, -146.888121f, -389.642741f);
        Vector c5 = new Vector(298.941929f, 151.947507f, 359.860766f);
        Vector c6 = new Vector(-92.697067f, -52.312119f, -123.143476f);

        // Utilizando las funciones internas de la clase Vector para operar
        return add(c0, mult(t, add( c1, mult(t, add( c2, mult(t, add( c3, mult(t, add( c4, mult(t, add(c5,mult(t,c6))))))))))));
    }


    //Run Sketch
    
    /*public void Cube(){
        
      return;
    }*/
   
    
    public void main(){
        
        String[] processingArgs = {"Cube"};
        Cube sketch3d = new Cube();
        PApplet.runSketch(processingArgs, sketch3d);
        
        return;
    }
    
    
    //Class

    class Clock{
        
        public float time = 0.0f;
        public float tick = 512.0f; //milliseconds
        public float tcurrent = 0.0f;
        public float tend = 0.0f;
        
        
        Clock(){
            init();
        }
        
        Clock(float tick){
            this.tick = tick;
            init();
        }
        
        void init(){
            this.tend = millis()+tick;
            return;
        }
        
        boolean compute(){
            this.tcurrent = millis();
            float dt = this.tend - this.tcurrent;
            this.time = (tick-dt)/1000.0f;
            return dt <= 0.0;
        }
        
    }//End-Class
        
    class Vector{
            
        public float x;
        public float y;
        public float z;
        public float w;
        
        Vector(){
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.w = 0;
        }
        
        Vector(float x, float y){
            this.x = x;
            this.y = y;
            this.z = 0;
            this.w = 0;
        }
        
        Vector(float x, float y, float z){
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = 0;
        }
        
        Vector(float x, float y, float z, float w){
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        
        
        Vector(float vec []){
            
            this.x = 0;
            this.y = 0;
            this.z = 0;
            this.w = 0;
            
            switch(vec.length){
            
            case 1: this.x = vec[0]; break;
            case 2: this.x = vec[0]; this.y = vec[1]; break;
            case 3: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; break;
            case 4: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; this.w = vec[3]; break;
            
            default: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; this.w = vec[3];
            
            }
            
            return;
        }
        
        
        float length(){
            return sqrt( (this.x*this.x) + (this.y*this.y) + (this.z*this.z) + (this.w*this.w) );
        }
        
        void add(float a){
            this.x += a;
            this.y += a;
            this.z += a;
            this.w += a; 
            return;
        }
        
        void mult(float a){
            this.x *= a;
            this.y *= a;
            this.z *= a;
            this.w *= a;
            return;
        }
        
        void normalize(){
            float d = this.length();
            this.mult(1.0f/d);
        }
        
        
        void setvalue(float x){
            this.x = x;
            return;
        }
        
        void setvalue(float x, float y){
            this.x = x;
            this.y = y;
            return;
        }
        
        void setvalue(float x, float y, float z){
            this.x = x;
            this.y = y;
            this.z = z;
            return;
        }
        
        void setvalue(float x, float y, float z, float w){
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
            return;
        }
        
        void setvalue(Vector v){
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
            this.w = v.w;
            return;
        }
        
        void setvalue(float vec []){
            
            switch(vec.length){
            case 1: this.x = vec[0]; break;
            case 2: this.x = vec[0]; this.y = vec[1]; break;
            case 3: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; break;
            case 4: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; this.w = vec[3]; break;
            default: this.x = vec[0]; this.y = vec[1]; this.z = vec[2]; this.w = vec[3];   
            }
            
            return;
        }
        
        float[] getvalue(){
            
            float values [] = {this.x,this.y,this.z,this.w};
            
            return values;
        }
            
            
    }//End-Class
        
    


}//End-Class







