
import processing.core.PApplet;
import processing.core.PVector;

public class Grid extends PApplet{
    

    //Processing Rutine
    
    public float time = 0.0f;
    public int dpi = 1;
    public float scalex = 50.0f;
    public float scaley = 50.0f;
    public String colorbackground = "#0E1012"; //14, 16, 18
    public String colorhud = "#E5DCDC"; //229, 220, 220

    @Override  
    public void settings(){
      
      size(900,505);
      pixelDensity(dpi);
      smooth(2);
      
      return;
    }

    @Override
    public void setup() {
        
      frameRate(60);
      textSize(64);
      textAlign(LEFT);
      rectMode (CENTER);
      
      return;  
    }
    
    
    @Override
    public void draw() {
      
      push();
      
      translate(width/2.0f, height/2.0f);
      scale(scalex,-scaley);
      
      updateconfig();
      
      background(colorbackground);
    
      float t = time;

      rotate(t*0.25f);
      scale(1.0f+0.25f*sin(t));
      
      dgrid(0,0, 500,500, 100,100, "#222223", 0.02f);
      dgrid(0,0, 100,100, 100,100, "#3F4042", 0.02f);
      daxis(0,0, 10,10, 0.035f, 0.1f, "#FFFFFF" );
      
      fill(255);
      dtext("A", 2.5f, 2.5f, 1.0f);
      dtext("B", -2.5f, 2.5f, 1.0f);
      dtext("C", -2.5f, -2.5f, 1.0f);
      dtext("D", 2.5f, -2.5f, 1.0f);

      pop();
      
      hud();
      return;
    }

    
    void updateconfig(){
      
      time = millis()/1000.0f;
      
      return;
    }

    void hud(){

      textSize(14.0f);
      fill(colorhud);
      noStroke();
      text(nfc(frameRate,2)+" fps", 5,16);
      text(nfc(millis()/1000.0f,2)+" s", 5,32);

      return;
    }
    
    
    //Draw functions

    void daxis(float x, float y, float lx, float ly){

      float thicknessline = 0.03f;
      float thicknesspoint = 0.1f;
      String colnegative = "#E4EBF4";
      String colxaxis = "#C03548";
      String colyaxis = "#4EAB30";

      //X
      stroke(colxaxis);
      strokeWeight (thicknesspoint);
      point(x+lx/2.0f,0.0f);

      strokeWeight (thicknessline);
      line(x-lx/2.0f,y,  x+lx/2.0f,y);

      stroke(colnegative);
      strokeWeight (thicknesspoint);
      point(x-lx/2.0f,0.0f);


      //Y
      stroke(colyaxis);
      strokeWeight (thicknesspoint);
      point(0.0f,y+ly/2.0f);

      strokeWeight (thicknessline);
      line(x,y-ly/2.0f,  x,y+ly/2.0f);

      stroke(colnegative);
      strokeWeight (thicknesspoint);
      point(0.0f,y-ly/2.0f);

      return;
    }

    void daxis(float x, float y, float lx, float ly, float thicknessline, float thicknesspoint, String colaxis){

      String colxpositive = "#FF4747"; //255, 71, 71
      String colypositive = "#7BFF47"; //123, 255, 71
      String colxaxis = colaxis;
      String colyaxis = colaxis;
      
      
      
      //x  
      stroke(colxaxis);
      strokeWeight (thicknessline);
      line(x-lx/2.0f,y,  x+lx/2.0f,y);

      strokeWeight (thicknesspoint);
      point(x-lx/2.0f,0.0f);

      stroke(colxpositive);
      strokeWeight (thicknesspoint);
      point(x+lx/2.0f,0.0f);

      //y  
      stroke(colyaxis);
      strokeWeight (thicknessline);
      line(x,y-ly/2.0f,  x,y+ly/2.0f);

      strokeWeight (thicknesspoint);
      point(0.0f,y-ly/2.0f);

      stroke(colypositive);
      strokeWeight (thicknesspoint);
      point(0.0f,y+ly/2.0f);

      return;
    }

    void dgrid(float x, float y, float col, float row, float w, float h, String linecolor, float linethickness){

      stroke(linecolor);
      strokeWeight(linethickness);

      //vertical lines
      for (float ic = -w/2.0f ; ic <= w/2.0f ; ic += w/col ){
        line (x+ic,y+-h/2.0f, x+ic , y+h/2.0f);    
      }

      //horizontal lines
      for (float ir = -h/2.0f ; ir <= h/2.0f ; ir += h/row ){    
        line (-w/2.0f + x , ir + y , w/2.0f + x , ir + y);
      }

      return;
    }

    void dtext(String str, float x, float y, float size){

      push();
      scale(1.0f,-1.0f);
      textSize(size);
      text(str,x,-y);
      pop();

      return;
    }

    
    //Utils
    
    public PVector hextorgb(String hexcolor) {
    
        PVector col = new PVector();

        if (hexcolor.charAt(0) == '#') {
            hexcolor = hexcolor.substring(1);
        }

        if (hexcolor.length() != 6) {
            throw new IllegalArgumentException("Invalid hex color format");
        }
        
        try {
            
            int _r = Integer.parseInt(hexcolor.substring(0, 2), 16);
            int _g = Integer.parseInt(hexcolor.substring(2, 4), 16);
            int _b = Integer.parseInt(hexcolor.substring(4, 6), 16);
            
            col.set(_r, _g, _b);

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid hex color format", e);
        }
        
        
        return col;
}
    
    void stroke(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        stroke(col.x, col.y, col.z);
     
        return;
    }
    
    void fill(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        fill(col.x, col.y, col.z);
     
        return;
    }
    
    void background(String hexcolor){
        
        PVector col = hextorgb(hexcolor);
        background(col.x, col.y, col.z);
     
        return;
    }
    
    
    //Run Sketch
    
    /*public void Grid(){
        
      return;
    }*/
   
    
    public void main(){
        
        String[] processingArgs = {"Grid"};
        Grid mySketch = new Grid();
        PApplet.runSketch(processingArgs, mySketch);
        
        return;
    }
    
    
}//End-Class
